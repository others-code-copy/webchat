# -*- coding: utf-8 -*-
"""
Created on Thu Feb 12 11:47:35 2015

@author: aaron
"""

import dbopt
import json

class chatcert(dbopt.dbopt):
    def __init__(self):
        dbopt.dbopt.__init__(self)
        self.enter_conect = []
        self.room_connect = []
        self.chat_connect = []
    def online(self,user,room_id,callback):
        self.add_online(user,room_id)
        self.enter_conect.append(callback)
    def outline(self,user,callback):
        self.set_outline(user)
        self.enter_conect.remove(callback)
        self.notifycallbacks()
    def notifycallbacks(self):
        print self.enter_conect
        for x in self.enter_conect:
            x.write_message(str(len(self.get_online())))
    def notify_room_callbacks(self,room_id):
        print self.room_connect
        for x in self.room_connect:
            x.write_message(json.dumps(self.user_in_room(room_id)))
    def user_enter_room(self,user,room_id,connect):
       #self.add_online(user,room_id)
        self.room_connect.append(connect)
        print room_id
        self.notify_room_callbacks(room_id)
    def user_leave_room(self,user,room_id,connect):
        #self.change_room(room_id,'5216',user)
        self.room_connect.remove(connect)
        self.notify_room_callbacks(room_id)
    def add_chat(self,connect):
        self.chat_connect.append(connect)
    def blocast_chat(self,chatinfo):
        for con in self.chat_connect:
            con.write_message(json.dumps(chatinfo))
    def leave_chat(self,connect):
        self.chat_connect.remove(connect)
        
        